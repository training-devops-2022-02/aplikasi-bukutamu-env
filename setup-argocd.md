# Setup ArgoCD #

1. Install ArgoCD

    ```
    kubectl create namespace argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    ```

2. Cek hasil instalasi

    ```
    kubectl get pods -n argocd
    kubectl get svc -n argocd
    ```

3. Expose service `argocd-server` supaya bisa diakses dari luar cluster

    ```
    kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
    ```

4. Melihat username/password user web UI

    ```
    kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
    ```

5. Mendaftarkan aplikasi di ArgoCD

    ```
    kubectl apply -f argocd-bukutamu-dev.yml
    ```

6. Mengetes sync dengan perubahan di repo, edit jumlah replica menjadi 2

    ```
    git add .
    git commit -m "menambah replica aplikasi menjadi 2"
    git push
    ```

7. Mengetes fitur self healing, set replica aplikasi bukutamu menjadi 10

    ```
    kubectl scale --replicas=10 deployment bukutamu-app-deployment-dev -n bukutamu-dev
    ```